const express = require('express');
const app = express.Router();
const fs = require('fs');

const FastGlob = require('fast-glob');

const ourFiles = FastGlob.sync(['docs/**/*.md'],{});
const depFiles = FastGlob.sync(['node_modules/**/docs/**/*.md'],{});
const allFiles = ourFiles.concat(depFiles);

app.get('/dev-docs/api',(req,res) => {
  res.json({ local: ourFiles,
             dependencies: depFiles });
});

app.get('/dev-docs/api/*',(req,res) => {
  const path = req.params[0];

  if(!allFiles.includes(path)) {
    res.status(401);
    console.log("401");
    res.send('not authorized');
    return;
  }

  console.log("sending");
  const stat = fs.statSync(path);

  res.status(200);
  res.set('Content-Type','text/markdown; charset=UTF-8');
  res.set('Content-Length',stat.size);

  fs.createReadStream(path).pipe(res);
});

module.exports = app;
