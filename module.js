const path = require('path')
const util = require('util');


module.exports = function (moduleOptions) {
  if(this.options.dev) {
    this.addLayout(path.resolve(__dirname,'layouts/dev-docs.vue'),'dev-docs');

    this.extendRoutes((routes,resolve) => {
      routes.push({
        path: '/dev-docs',
        component: resolve(__dirname,'pages/dev-docs.vue'),
        children: [
          { path: '',
            component: resolve(__dirname,'pages/dev-docs/index.vue'),
          }
        ]
      });
    });

    this.addServerMiddleware(path.resolve(__dirname,'./server/docs-loader.js'));
  }
};
